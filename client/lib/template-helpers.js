Template.registerHelper("createdAtFormatted", function(createdAt) {
  return createdAt ? moment(createdAt).format('DD-MM-YYYY HH:mm') : '–';
});

Template.registerHelper("dueAtFormatted", function(dueAt) {
  return dueAt ? moment(dueAt).format('DD-MM-YYYY') : '–';
});
