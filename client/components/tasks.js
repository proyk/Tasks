Template.Tasks.onCreated(function () {
  const currentUser = Meteor.user();

  this.tasksLimit = new ReactiveVar(5);

  this.getCurrentUserClassroomId = () => {
    return currentUser && currentUser.profile.classroomId ? currentUser.profile.classroomId : "";
  }

  this.getUserId = () => {
    return currentUser ? currentUser._id : "";
  }

  this.autorun( () => {
    const terms = {
      classroomId: this.getCurrentUserClassroomId(),
      userId: this.getUserId(),
      limit: this.tasksLimit.get()
    };

    this.subscribe('Tasks_inList', terms);
  })
});

Template.Tasks.helpers({
  tasks: function () {
    return Tasks.find({}, {sort: {createdAt: -1}});
  },
  hasMoreLimit: function () {
    const currentLimit = Template.instance().tasksLimit.get();
    const tasksCount = Tasks.find().count();
    return currentLimit == tasksCount;
  }
});

Template.Tasks.events({
  'click #load-more': function () {
    // increment the counter when button is clicked
    const instance = Template.instance();
    const currentLimit = instance.tasksLimit.get();

    instance.tasksLimit.set(currentLimit + 1);
  }
});
