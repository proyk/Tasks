Template.Master_layout.onCreated(function(){
  var self = this;

  self.minHeight = new ReactiveVar($(window).height() - $('.main-header').height() - 51);

  $(window).resize(function () {
    self.minHeight.set($(window).height() - $('.main-header').height());
  });

  $('body').addClass('fixed');

});

Template.Master_layout.helpers({
  minHeight: function () {
    return Template.instance().minHeight.get() + 'px';
  }
});
