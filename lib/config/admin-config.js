AdminConfig = {
  name: 'Tasks Admin',
  adminEmails: ['admin@meteor.com'],
  userSchema: new SimpleSchema({
    'profile.name': {
      type: String
    },
    'profile.classroomId': {
      type: String,
      label: 'Classroom',
      optional: true,
      autoform: {
        options: function () {
          return _.map(Classrooms.find().fetch(), function (classroom) {
            return {
              label: classroom.name,
              value: classroom._id
            };
          });
        }
      }
    }
  }),
  collections: {
    Classrooms: {
      icon: 'edit',
      omitFields: ['createdAt', 'owner'],
      tableColumns: [
        { label: 'Classroom', name: 'name' },
        { label: 'Description', name: 'description' },
        { label: 'CreatedAt', name: 'createdAt'}
      ],
      color: 'red'
    },
    Tasks: {
      icon: 'tasks',
      omitFields: ['classroomName','createdAt', 'owner'],
      tableColumns: [
        { label: 'Title', name: 'title' },
        { label: 'Subject', name: 'subject' },
        { label: 'details', name: 'details' },
        { label: 'Classroom', name: 'classroomName' },
        { label: 'CreatedAt', name: 'createdAt' },
        { label: 'DueAt', name: 'dueAt' },
      ],
      color: 'green'
    }
  },
  skin: 'green'
};
