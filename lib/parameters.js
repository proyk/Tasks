getTasksParameters = function({ classroomId, userId, limit }) {
  var baseParameters = {
    find: {},
    options: {
      sort: {createdAt: -1},
      limit: parseInt(limit)
    }
  };

  const hasRootRole = () => {
    return Roles.userIsInRole(userId, 'admin');
  }

  var parameters = baseParameters;

  if (hasRootRole()) {
    _.extend(parameters.find, {'owner._id': userId});
  } else {
    _.extend(parameters.find, {classroomId: classroomId});
  }

  return parameters;
};
