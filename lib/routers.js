Router.configure({
  layoutTemplate: 'Master_layout',
  yieldTemplates: {
    'Nav': {to: 'nav'},
    'Footer': {to: 'footer'},
  },
  waitOn() {
    //Subs all classrooms for user classroom update
    return Meteor.subscribe('Classrooms_forUserEdit');
  }
});

Router.route('/', function () {
  this.render('Tasks');

  // render the PostAside template into the yield region named "aside"
  // {{> yield "aside"}}
  this.render('Nav', {to: 'nav'});

  // render the PostFooter template into the yield region named "footer"
  // {{> yield "footer"}}
  this.render('Footer', {to: 'footer'});
}, {
  name: 'dashboard'
});

Router.plugin('dataNotFound', {notFoundTemplate: 'Page_not_found'});

Router.plugin('ensureSignedIn', {
  except: ['signin']
});

AccountsTemplates.configureRoute('changePwd');
AccountsTemplates.configureRoute('forgotPwd');
AccountsTemplates.configureRoute('resetPwd');
AccountsTemplates.configureRoute('signIn');
AccountsTemplates.configureRoute('signUp');
AccountsTemplates.configureRoute('verifyEmail');
