Tasks = new Meteor.Collection('tasks');

Tasks.schema = new SimpleSchema({
  title: {
    type: String,
    max: 20
  },
  subject: {
    type: String,
    max: 100
  },
  details: {
    type: String,
    autoform: {
      rows: 5
    },
    max: 1000
  },
  classroomId: {
    type: String,
    label: 'Classroom',
    autoform: {
      options: function () {
        return _.map(Classrooms.find().fetch(), function (classroom) {
          return {
            label: classroom.name,
            value: classroom._id
          };
        });
      }
    }
  },
  classroomName: {
    type: String,
    autoValue: function (doc) {
      if (this.isUpdate) {
        const classroom = Classrooms.findOne(doc.$set.classroomId, {fields: {name: 1}});
        return classroom.name;
      };
      if (this.isInsert) {
        const classroom = Classrooms.findOne(doc.classroomId, {fields: {name: 1}});
        return classroom.name;
      };
    }
  },
  owner: {
    type: Object,
    blackbox: true,
    label: 'Owner',
    autoValue: function () {
      if (this.isInsert) {
        const currentUser = Meteor.user();
        return {_id: currentUser._id, name: currentUser.profile.name};
      }
    }
  },
  createdAt: {
    type: Date,
    label: 'Created At',
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      }
    }
  },
  dueAt: {
    type: Date,
    label: 'Due At'
  }
});

Tasks.attachSchema(Tasks.schema);
