Classrooms = new Meteor.Collection('classrooms');

Classrooms.schema = new SimpleSchema({
  name: {
    type: String,
    max: 60
  },
  description: {
    type: String,
    autoform: {
      rows: 1
    },
    max: 50
  },
  createdAt: {
    type: Date,
    label: 'Date',
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      }
    }
  },
  owner: {
    type: Object,
    blackbox: true,
    label: 'Owner',
    autoValue: function () {
      if (this.isInsert) {
        const currentUser = Meteor.user();
        return {_id: currentUser._id, name: currentUser.profile.name};
      }
    }
  }
});

Classrooms.attachSchema(Classrooms.schema);

Classrooms.helpers({
  classroomName: function() {
    return Classroom.findOne(this._id).name;
  }
});
