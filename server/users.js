Accounts.onCreateUser(function (options, user) {
  if (options.profile) {
    // include the user profile
    user.profile = options.profile;
  }

  return user;
});

Meteor.startup( function() {
  if (Meteor.users.find().count() === 0) {
    var _id = Accounts.createUser({
      email: "admin@meteor.com",
      password: "123456",
      createdAt: new Date(),
      profile: {
        name: "Admin",
      }
    });
    Roles.addUsersToRoles(_id, ['admin']);
  }
});
