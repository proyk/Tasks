Meteor.publish('Classrooms_forUserEdit', function () {
  if (!this.userId) {
    return this.ready();
  }

  return Classrooms.find({}, {
    fields: {name:1}
  });
});

Meteor.publish('Tasks_inList', function (terms) {
  if (!this.userId) {
    return this.ready();
  }
  const params = getTasksParameters(terms);

  return Tasks.find(params.find, params.options);
});
